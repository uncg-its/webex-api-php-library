<?php

namespace Uncgits\WebexApi;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Uncgits\WebexApi\Exceptions\WebexApiException;

class WebexApi
{

    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    |
    | For abstraction these properties will hold the relevant environment and
    | auth info. Set these in a wrapper or whatever other framework you're
    | using.
    |
    */

    protected $xmlHost; // host only, no protocols
    protected $nbrHost; // host only, no protocols

    protected $username;
    protected $password;
    protected $siteName;
    protected $partnerId;

    protected $applicationName;

    protected $proxyHost;
    protected $proxyPort;

    protected $useProxy = false;

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */

    /**
     * Sets the $xmlHost attribute
     *
     * @param string $xmlHost - the host string for the XML API
     *
     * @return void
     */
    public function setXmlHost($xmlHost)
    {
        $this->xmlHost = $xmlHost;
    }

    /**
     * Sets the $nbrHost attribute
     *
     * @param string $nbrHost - the host string for the NBR API
     *
     * @return void
     */
    public function setNbrHost($nbrHost)
    {
        $this->nbrHost = $nbrHost;
    }

    /**
     * Sets the $username attribute
     *
     * @param string $username - username for the account making the API call
     *
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Sets the $password attribute
     *
     * @param string $password - password for the account making the API call
     *
     * @return void
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Sets the $siteName attribute
     *
     * @param string $siteName - Webex site name (usually * in *.webex.com)
     *
     * @return void
     */
    public function setSiteName($siteName)
    {
        $this->siteName = $siteName;
    }

    /**
     * Sets the $partnerId attribute
     *
     * @param string $partnerId - partner ID for the Webex site (deprecated)
     *
     * @return void
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
    }

    /**
     * Sets the $applicationName attribute
     *
     * @param string $applicationName - name of the application calling the API
     *
     * @return void
     */
    public function setApplicationName($applicationName)
    {
        $this->applicationName = $applicationName;
    }

    /**
     * Sets the $proxyHost attribute
     *
     * @param string $proxyHost - hostname of the HTTP proxy
     *
     * @return void
     */
    public function setProxyHost($proxyHost)
    {
        $this->proxyHost = $proxyHost;
    }

    /**
     * Sets the $proxyPort attribute
     *
     * @param mixed $proxyPort - port for the HTTP proxy
     *
     * @return void
     */
    public function setProxyPort($proxyPort)
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * Sets the $useProxy attribute
     *
     * @param boolean $useProxy - whether to use the proxy
     *
     * @return void
     */
    public function setUseProxy($useProxy)
    {
        $this->useProxy = ($useProxy === true);
    }

    /*
    |--------------------------------------------------------------------------
    | API Calls
    |--------------------------------------------------------------------------
    |
    | Methods that actually perform the API calls to WebEx APIs. Not callable
    | directly; only internally by other class methods.
    |
    */

    /**
     * Performs a call to the WebEx XML API
     *
     * @param string $service     service category (per WebEx API docs)
     * @param string $request     request endpoint (per WebEx API docs)
     * @param string $bodyContent XML to provide inside <bodyContent> tag for the call
     * @param string $asUser      (optional) the username with which to perform the API call, if not the administrator
     *
     * @return array
     */
    protected function xmlApiCall($service, $request, $bodyContent = '', $asUser = null)
    {
        $requiredProperties = ['xmlHost', 'username', 'password', 'siteName', 'partnerId', 'applicationName'];
        foreach ($requiredProperties as $property) {
            if ($this->$property === null) {
                throw new \Exception("Error: required property '$property' has not been set in API object.");
            }
        }

        // assemble the request target and endpoint
        $requestTarget = "$service.$request";
        $endpoint = 'https://' . $this->xmlHost . '/WBXService/XMLService';

        // run the call as another user? this is required for some operations (e.g. recording trash)
        if (!is_null($asUser)) {
            // assign temporary password
            $newPass = Helpers::getNewSaltedPassword();
            $newRawPass = $newPass['rawPassword'];

            $changePassResult = $this->changeUserPassword($asUser, $newRawPass);
            if ($changePassResult['response']['result'] != 'SUCCESS') {
                throw new \Exception('Error: could not change user password. Reason: ' . $changePassResult['response']['message']);
            }

            // continue using the temporary password
            $username = $asUser;
            $password = $newRawPass;
        } else {
            $username = $this->username;
            $password = $this->password;
        }

        // define request XML
        $xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <header>
        <securityContext>
            <webExID>$username</webExID>
            <password>$password</password>
            <siteName>{$this->siteName}</siteName>
            <partnerID>{$this->partnerId}</partnerID>
        </securityContext>
    </header>
    <body>
        <bodyContent xsi:type="java:com.webex.service.binding.$requestTarget">
        $bodyContent
        </bodyContent>
    </body>
</serv:message>
XML;

        // instantiate Guzzle client with headers
        $client = new Client([
            'headers' => [
                'POST /WBXService/XMLService HTTP/1.0',
                "Host: {$this->xmlHost}",
                "User-Agent: {$this->applicationName}"
            ]
        ]);

        // set up basic options
        $requestOptions = [
            'Content-Type' => 'text/xml; charset=UTF8',
            'body'         => $xmlRequest
        ];

        // use proxy if it is set in .env
        if ($this->useProxy) {
            $requestOptions['proxy'] = $this->proxyHost . ':' . $this->proxyPort;
        } else {
            $requestOptions['proxy'] = '';
        }

        // perform the call
        $response = $client->post($endpoint, $requestOptions);

        // get response data
        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = Helpers::xml2array($body->getContents()); // parse XML to array

        // parse basic info for all requests
        $servResponse = $bodyContents['serv:message']['serv:header']['serv:response'];

        $servResult = $servResponse['serv:result'];
        if ($servResult == 'SUCCESS') {
            $servMessage = 'Successful API call';
            $servExceptionId = null;
        } else {
            $servMessage = isset($servResponse['serv:reason']) ? $servResponse['serv:reason'] : 'Unknown error.';
            $servExceptionId = isset($servResponse['serv:exceptionID']) ? $servResponse['serv:exceptionID'] : 'unknown exception ID';
        }

        // define standardized information about this call.

        $result = [
            'request'  => $bodyContent,
            'response' => [
                'endpoint'    => $endpoint,
                'target'      => $requestTarget,
                'method'      => 'POST',
                'result'      => $servResult,
                'message'     => $servMessage,
                'exceptionId' => $servExceptionId,
                'httpCode'    => $code,
                'httpReason'  => $reason
            ],
            'body'     => $servResult == 'SUCCESS' ? $bodyContents['serv:message']['serv:body']['serv:bodyContent'] : false
        ];

        if ($servResult !== 'SUCCESS') {
            throw new WebexApiException(null, null, null, $result);
        }

        return $result;
    }


    protected function nbrApiCall($endpoint, $service, $bodyContent = '')
    {
        // TODO - this
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    | Methods that perform common operations between other outward-facing
    | methods (e.g. getting a list of users)
    |
    */

    /**
     * Retrieves a list of users from WebEx, as a collection, keyed by username
     *
     * @param string $active        Subset filter: 'all', 'active', 'inactive'
     * @param string $numberToGet   Number of records to get
     * @param string $sortBy        Which field to sort by
     * @param string $sortDirection Which direction to sort by
     * @param bool $raw             Whether to return raw (not used)
     *
     * @return array
     */
    private function getUsersList(
        $active = 'all',
        $numberToGet = 'all',
        $sortBy = 'UID',
        $sortDirection = 'ASC',
        $raw = false
    ) {
        // NOTES
        // if $startFrom is too high, it'll give FAILURE message "Sorry, no record found."
        // negative values passed into WebEx API will be set to defaults (startFrom to 1, maximumNum to default 500).

        // <orderBy> will take UID, FIRSTNAME, LASTNAME, EMAIL, REGDATE
        // <orderAD> will take ASC or DESC

        $webexUserRequestMaxSize = 500; // set by WebEx. Won't change unless their API changes.

        // $filter should be "active", "inactive"
        if ($active == 'active') {
            $extraXML = "<active>ACTIVATED</active>";
        } else {
            if ($active == 'inactive') {
                $extraXML = "<active>DEACTIVATED</active>";
            } else {
                $extraXML = '';
            }
        }

        // validate sorting
        $validSortCriteria = ['UID', 'FIRSTNAME', 'LASTNAME', 'EMAIL', 'REGDATE'];
        if (!in_array($sortBy, $validSortCriteria)) {
            $sortBy = 'UID';
        }

        $validSortDirections = ['ASC', 'DESC'];
        if (!in_array($sortDirection, $validSortDirections)) {
            $sortDirection = 'ASC';
        }

        // initialize stuff for first call
        $currentPosition = 1;
        $fetchNumber = $webexUserRequestMaxSize; // actual fetch number - set here for initial call. only less than the max if specifically asked for less than the max.

        if (is_numeric($numberToGet)) {
            if ($numberToGet < $webexUserRequestMaxSize) {
                $fetchNumber = $numberToGet;
            }
            $stopAt = $numberToGet;
        } else {
            $stopAt = 0; // if not given a stop point, it'll be all results, which we can set at the same time as $resultSetSize.
        }

        $resultSetSize = 0; // to be set later once we know this value from the first API call.

        //$fetchNumber = 1; // testing

        // init result array
        $returnArray = [];

        // loop it!
        do {
            // set XML
            $bodyContent = <<<XML
            <order>
                <orderBy>$sortBy</orderBy>
                <orderAD>$sortDirection</orderAD>
            </order>
            <listControl>
                <startFrom>$currentPosition</startFrom>
                <maximumNum>$fetchNumber</maximumNum>
                <listMethod>AND</listMethod>
            </listControl>
            $extraXML
XML;

            // execute call
            $result = $this->xmlApiCall('user', 'LstsummaryUser', $bodyContent);

            $serverResponse = $result['response'];
            $body = $result['body'];

            // process
            if ($serverResponse['result'] == 'SUCCESS') {
                // grab the users and append them to the returnArray.
                $usersReturned = $body['use:user'];

                // if the call retrieves multiple records, use:user will be an array of arrays with user info.
                // if the call retrieves a single record, use:user will be only the array with user info.
                // we can get around this by looking at how many records webex tells us it's giving us.

                if ($body['use:matchingRecords']['serv:returned'] == 1) {
                    // this is a singleton
                    array_push($returnArray, $usersReturned);
                } else {
                    // grouping
                    $returnArray = array_merge($returnArray, $usersReturned);
                }

                if ($resultSetSize == 0) {
                    // if this hasn't been set yet, then set it now.
                    $resultSetSize = $body['use:matchingRecords']['serv:total'];
                    if ($stopAt == 0) {
                        // set this also if it wasn't set before. this'll stop at the size of the resultset.
                        $stopAt = $resultSetSize;
                    }
                }
            } else {
                $stopAt = $currentPosition; // force the loop to stop.
            }

            $currentPosition += $fetchNumber; // in case pagination is needed. if it's not needed this won't matter anyway.

            $arrayCount = count($returnArray);
            if ($stopAt - $arrayCount < $webexUserRequestMaxSize) {
                $fetchNumber = $stopAt - $arrayCount;
            }
        } while ($currentPosition < $stopAt);


        // append count of data to server response info
        $serverResponse['recordsFetched'] = count($returnArray);

        return [
            'response' => $serverResponse,
            'data'     => $returnArray
        ];
    }

    /**
     * Adds a user to Webex
     *
     * @param string $firstName     User first name
     * @param string $lastName      User last name
     * @param string $username      User username / Webex ID
     * @param string $email         User email address
     * @param string $password      User password (required, use temp if needed)
     * @param string $additionalXml Additional XML for user privs, etc.
     *
     * @return array
     */
    public function addUser($firstName, $lastName, $username, $email, $password, $additionalXml = '')
    {

        // define base xml
        $bodyContent = <<<XML
            <firstName>$firstName</firstName>
            <lastName>$lastName</lastName>
            <webExId>$username</webExId>
            <email>$email</email>
            <password>$password</password>
            <active>ACTIVATED</active>
            $additionalXml
XML;
        // make API call
        return $this->xmlApiCall('user', 'CreateUser', $bodyContent);
    }

    /**
     * Deletes a user from Webex (sets to inactive)
     *
     * @param string $username - the username of the Webex user
     *
     * @return array
     */
    public function deleteUser($username)
    {
        $bodyContent = <<<XML
    <webExId>$username</webExId>
XML;

        return $this->xmlApiCall('user', 'DelUser', $bodyContent);
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx Site Methods
    |--------------------------------------------------------------------------
    |
    | Operations that affect / reference the WebEx Site as a whole / global
    | settings
    |
    */

    /**
     * Gets the WebEx site information (site.GetSite)
     *
     * @return array
     */
    public function getSiteInfo()
    {
        return $this->xmlApiCall('site', 'GetSite');
    }

    /**
     * Gets the XML API version for the site (ep.GetAPIVersion)
     *
     * @return array
     */
    public function getXmlApiVersion()
    {
        return $this->xmlApiCall('ep', 'GetAPIVersion');
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx User Information Methods (get)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with listing / retrieving user information
    |
    */

    /**
     * Gets all users from WebEx, with both ACTIVATED and DEACTIVATED status
     *
     * @return array
     */
    public function getAllUsers()
    {
        return $this->getUsersList(); // default params will suffice
    }

    /**
     * Gets all users from WebEx with ACTIVATED status only
     *
     * @return array
     */
    public function getActiveUsers()
    {
        return $this->getUsersList('active');
    }

    /**
     * Gets details on a user
     *
     * @param string $username - username / Webex ID of the user
     *
     * @return array
     */
    public function getUser($username)
    {
        $bodyContent = <<<XML
            <webExId>$username</webExId>
XML;
        return $this->xmlApiCall('user', 'GetUser', $bodyContent);
    }

    /**
     * Returns two checks - whether a user exists in WebEx, and whether the user
     * is currently ACTIVATED
     *
     * @param string $webExId - username / Webex ID of the user
     *
     * @return array
     */
    public function getUserStatus($webExId)
    {
        $result = $this->getUser($webExId);

        // init
        $exists = false;
        $active = false;

        if ($result["response"]["result"] == "SUCCESS") {
            $exists = true;
            if ($result["body"]["use:active"] == "ACTIVATED") {
                $active = true;
            }
        }

        return [
            'response' => $result,
            'exists'   => $exists,
            'active'   => $active
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx User Action Methods (add / edit / delete)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with modifying user information
    |
    */


    /**
     * Changes a user's API password
     *
     * @param string $username - the username / Webex ID of the user
     * @param string $password - the new password
     *
     * @return array
     */
    public function changeUserPassword($username, $password)
    {
        $xml = <<<XML
<password>$password</password>
XML;
        return $this->editUser($username, [], $xml);
    }

    /**
     * Changes a user's attributes in WebEx
     *
     * @param string $username   - the username / Webex ID of the user
     * @param array  $attributes - attributes to change
     * @param null   $customXml  - additional XML to be appended to $bodyContent
     *
     * @return array
     */
    public function editUser($username, $attributes = [], $customXml = null)
    {

        // init XML
        $bodyContent = "<webExId>$username</webExId>";
        $bodyContent .= "<active>ACTIVATED</active>";

        if (count($attributes) > 0) {
            $permittedAttributes = ["firstName", "lastName", "email", "newWebExId"];
            foreach ($attributes as $attribute => $value) {
                if (in_array($attribute, $permittedAttributes)) {
                    $bodyContent .= "<$attribute>$value</$attribute>";
                }
            }
        }
        if (!is_null($customXml)) {
            // allow for custom XML
            $bodyContent .= $customXml;
        }

        return $this->xmlApiCall("user", "SetUser", $bodyContent);
    }

    /**
     * Changes a user's schedulingPermission attribute in WebEx (allow other
     * users to schedule for the user)
     *
     * @param string $username        - the username / Webex ID of the user
     * @param array  $permissionArray - array of usernames
     *
     * @return array
     */
    public function setUserSchedulingPermission($username, $permissionArray)
    {
        if (!is_array($permissionArray)) {
            $permissionArray = [$permissionArray];
        }

        $permissionList = implode(';', $permissionArray);

        $xml = <<<EOD
<schedulingPermission>$permissionList</schedulingPermission>
EOD;
        return $this->editUser($username, [], $xml);
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx Recording Information Methods (get)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with listing / retrieving recording information
    |
    */
    /**
     * Gets a list of recordings from the API
     *
     * @param array $options - options for the call (see below)
     *
     * @return array
     */
    public function getRecordings($options = [])
    {
        /*
         * $options structure:
            'webExId'           => username of webex user to pull
            'maximumNum'        => max number of recordings to pull
            'createTimeStart'   => time created lower bound
            'createTimeEnd'     => time created upper bound
            'serviceTypes'      => array containing strings: MeetingCenter,
                EventCenter, SupportCenter, TrainingCenter
            'confId'            => pull only for a specific confID
            'historyDays'       => days of history to pull if specific dates are
                not provided (WebEx allows up to 28)
        */

        $historyDays = isset($options['historyDays']) ? $options['historyDays'] : 28;

        $now = new Carbon();
        $start = new Carbon();
        $start->subDays($historyDays);

        // need them in GMT
        $createTimeEnd = gmdate('m/d/Y 00:00:00', $now->timestamp);
        $createTimeStart = gmdate('m/d/Y 23:59:59', $start->timestamp);

        $defaults = [
            'maximumNum'      => 500, // webex max value as of 4/2016
            'createTimeStart' => $createTimeStart,
            'createTimeEnd'   => $createTimeEnd,
            'webExId'         => null,
            'serviceTypes'    => null,
            'confId'          => null,
        ];

        // set params to defaults if we don't get them passed in.
        foreach ($defaults as $key => $val) {
            if (!isset($options[$key])) {
                $options[$key] = $val;
            }
        }


        // if set, go with the set value. otherwise default.
        $webExId = (!is_null($options['webExId'])) ? "<hostWebExID>{$options['webExId']}</hostWebExID>" : '';
        $confId = (!is_null($options['confId'])) ? "<confID>{$options['confId']}</confID>" : '';


        $createTimeStart = (isset($options['createTimeStart'])) ? "<createTimeStart>{$options['createTimeStart']}</createTimeStart>" : '';
        $createTimeEnd = (isset($options['createTimeEnd'])) ? "<createTimeEnd>{$options['createTimeEnd']}</createTimeEnd>" : '';

        $createTimeScope = '';
        if (!empty($createTimeStart) || !empty($createTimeEnd)) {
            $createTimeScope = <<<XML
    <createTimeScope>
        $createTimeStart
        $createTimeEnd
    </createTimeScope>
XML;
        }

        if (!is_null($options['serviceTypes']) && is_array($options['serviceTypes'])) {
            $serviceTypes = '<serviceTypes>';
            foreach ($options['serviceTypes'] as $s) {
                $serviceTypes .= "<serviceType>$s</serviceType>";
            }
            $serviceTypes .= '</serviceTypes>';
        } else {
            $serviceTypes = '';
        }

        $bodyContent = <<<XML
    <listControl>
        <startFrom>0</startFrom>
        <maximumNum>{$options['maximumNum']}</maximumNum>
    </listControl>
    $createTimeScope
    $webExId
    $serviceTypes
    $confId
    <returnSessionDetails>TRUE</returnSessionDetails>
XML;

        $result = $this->xmlApiCall('ep', 'LstRecording', $bodyContent);

        $serverResponse = $result['response'];

        if (isset($result['body']['ep:recording']['ep:name'])) {
            // cast single result to array
            $result['body']['ep:recording'] = [$result['body']['ep:recording']];
        }
        $data = $result['body'];

        return [
            'response' => $serverResponse,
            'data'     => $data
        ];
    }

    /**
     * Looks up the most recent recording list and tries to parse it for the
     * given ID. if it fails to find that recording in the default time window,
     * it will fall back to the two-call way to do this, using getOldRecording().
     *
     * @param $id
     *
     * @return array
     */
    public function getRecording($id)
    {
        // try and save some API bandwidth by just doing one call instead of two.
        $recordingsRequest = $this->getRecordings();

        if ($recordingsRequest['response']['result'] == 'SUCCESS') {
            // loop through and figure out which is the right one.
            foreach ($recordingsRequest['data']['ep:recording'] as $recording) {
                if ($recording['ep:recordingID'] == $id) {
                    return [
                        'response' => $recordingsRequest['response'],
                        'data'     => ['ep:recording' => $recording]
                    ];
                }
            }
        }

        // fallback - do a lookup on an older record
        return $this->getOldRecording($id);
    }

    /**
     * Look up a recording by id - this works no matter when the recording was
     * created. two API calls are made in order to do this accurately, and so
     * getRecording() is preferred - hence this method is private.
     *
     * @param $id
     *
     * @return array
     */
    private function getOldRecording($id)
    {
        $bodyContent = <<<XML
    <recordingID>$id</recordingID>
XML;
        // because of the API, we need to do THIS to look up by recording ID.
        // then, we can use the session name it gives us to look up the ACTUAL
        // information about the recording.

        $recInfoResult = $this->xmlApiCall('ep', 'GetRecordingInfo', $bodyContent);

        if ($recInfoResult['response']['result'] == 'SUCCESS') {
            $title = $recInfoResult['body']['ep:basic']['ep:topic'];

            // encode the ampersands
            $title = str_replace('&', '&amp;', $title);

            $bodyContent = <<<XML
    <recordName>$title</recordName>
XML;
            $result = $this->xmlApiCall('ep', 'LstRecording', $bodyContent);

            $serverResponse = $result['response'];
            $data = $result['body'];
        } else {
            $serverResponse = $recInfoResult['response'];
            $data = null;
        }

        // because it's a fuzzy name search, we need to re-verify the recording
        // ID if multiple records are returned so that we give back the right
        // one.

        if (isset($data['ep:recording'][0])) {
            // we need to parse these to find the right one - verify the recording ID
            $correctRecord = null;
            foreach ($data['ep:recording'] as $rec) {
                if ($rec['ep:recordingID'] == $id) {
                    $correctRecord = $rec;
                }
            }
            // modify the original call response
            $data['ep:recording'] = $correctRecord;
        }

        return [
            'response' => $serverResponse,
            'data'     => $data
        ];
    }

    public function getTrashedRecordingsForUser($webExId)
    {
        return $this->xmlApiCall('ep', 'LstRecordingInRecycleBin', '', $webExId);
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx Recording Action Methods (add / edit / delete)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with modifying recording information
    |
    | TODO - add this in
    |
    */

    /*
    |--------------------------------------------------------------------------
    | WebEx Session Information Methods (get)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with listing / retrieving session information
    |
    | TODO - add this in
    |
    */

    public function getScheduledSessions($options = [])
    {
        $now = new Carbon();
        $end = new Carbon();
        $end->addDays(14); // default is 1 year out

        // need them in GMT
        $startDateEnd = gmdate('m/d/Y 23:59:59', $end->timestamp);
        $startDateStart = gmdate('m/d/Y 00:00:00', $now->timestamp);

        $defaults = [
            'maximumNum'     => 500, // webex max value as of 4/2016
            'startDateStart' => $startDateStart,
            'startDateEnd'   => $startDateEnd,
            'webExId'        => null,
            'centers'        => ['mc', 'tc', 'ec'],
            'collated'       => false
        ];

        // set params to defaults if we don't get them passed in.
        foreach ($defaults as $key => $val) {
            if (!isset($options[$key])) {
                $options[$key] = $val;
            }
        }

        $endpointsToQuery = [];

        foreach ($options['centers'] as $c) {
            switch ($c) {
                case 'mc':
                    $endpointsToQuery[$c] = [
                        'endpoint' => 'LstsummaryMeeting',
                        'service'  => 'meeting'
                    ];
                    break;
                case 'tc':
                    $endpointsToQuery[$c] = [
                        'endpoint' => 'LstsummaryTrainingSession',
                        'service'  => 'training'
                    ];
                    break;
                case 'ec':
                    $endpointsToQuery[$c] = [
                        'endpoint' => 'LstsummaryEvent',
                        'service'  => 'event'
                    ];
                    break;
            }
        }

        $data = [];

        $errorMessages = [];

        $webExId = $options['webExId'] ? "<hostWebExID>{$options['webExId']}</hostWebExID>" : '';

        foreach ($endpointsToQuery as $c => $e) {
            $orderBy = 'STARTTIME';

            $bodyContent = <<<XML
<listControl>
    <startFrom>0</startFrom>
    <maximumNum>{$options['maximumNum']}</maximumNum>
</listControl>
<order>
    <orderBy>$orderBy</orderBy>
    <orderAD>ASC</orderAD>
</order>
<dateScope>
    <startDateStart>{$options['startDateStart']}</startDateStart>
    <startDateEnd>{$options['startDateEnd']}</startDateEnd>
</dateScope>
$webExId
XML;

            $result = $this->xmlApiCall($e['service'], $e['endpoint'], $bodyContent);

            $serverResponse = $result['response']['result'];

            if ($serverResponse == 'SUCCESS') {
                $data[$c] = $result['body'];
            } else {
                $errorMessages[] = "error retrieving center schedule for endpoint {$e['endpoint']}. error: {$result['response']['message']}";
            }
        }

        if ($options['collated'] !== true) {
            $sessionsCombined = [];
            foreach ($data as $center => $sessions) {
                switch ($center) {
                    case 'mc':
                        $centerName = 'meeting';
                        $prefix = 'meet';
                        $endpoint = 'meeting';
                        break;
                    case 'tc':
                        $centerName = 'training';
                        $prefix = 'train';
                        $endpoint = 'trainingSession';
                        break;
                    case 'ec':
                        $centerName = 'event';
                        $prefix = 'event';
                        $endpoint = 'event';
                        break;
                    default:
                        $centerName = '';
                        $endpoint = '';
                        break;
                }


                // correct for single record
                if (isset($sessions[$prefix . ':' . $endpoint][$prefix . ':hostWebExID'])) {
                    $sessions[$prefix . ':' . $endpoint] = [$sessions[$prefix . ':' . $endpoint]];
                }

                foreach ($sessions[$prefix . ':' . $endpoint] as $session) {
                    // strip prefixes for easier iteration
                    $keys = array_map(function ($str) use ($prefix) {
                        $new = str_replace($prefix . ':', '', $str);

                        // normalize a few things
                        if ($new == 'meetingKey') {
                            return 'sessionKey';
                        } else {
                            if ($new == 'confName') {
                                return 'sessionName';
                            } else {
                                if ($new == 'meetingType') {
                                    return 'sessionType';
                                }
                            }
                        }


                        return $new;
                    }, array_keys($session));

                    $values = array_values($session);

                    $session = array_combine($keys, $values);

                    // add center name
                    $session['center'] = $centerName;

                    // add
                    $sessionsCombined[] = $session;
                }
            }
            $data = $sessionsCombined;
        }

        return [
            'response'      => 'SUCCESS',
            'data'          => $data,
            'errorMessages' => $errorMessages
        ];
    }

    public function getPastSessions($options = [])
    {
        // dates formatted as MM/dd/yyyy HH:mm:ss format

        /*
         * $options structure:
            'maximumNum'        => max number of meetings to pull
            'createTimeStart'   => time created lower bound
            'createTimeEnd'     => time created upper bound
            'webExId'           => username of webex user to pull
            'centers'           => array of abbreviations for centers to pull
            'collated'          => boolean, organize by center if true. otherwise just one blob with a field indicating center
            'historyDays'       => days of history to pull if specific dates are not provided
        */

        $historyDays = isset($options['historyDays']) ? $options['historyDays'] : 30;

        $now = new Carbon();
        $start = new Carbon();
        $start->subdays($historyDays);

        // need them in GMT
        $createTimeEnd = gmdate('m/d/Y 00:00:00', $now->timestamp);
        $createTimeStart = gmdate('m/d/Y 23:59:59', $start->timestamp);

        $defaults = [
            'maximumNum'      => 500, // webex max value as of 4/2016
            'createTimeStart' => $createTimeStart,
            'createTimeEnd'   => $createTimeEnd,
            'webExId'         => null,
            'centers'         => ['mc', 'tc', 'ec', 'sc'],
            'collated'        => false
        ];

        // set params to defaults if we don't get them passed in.
        foreach ($defaults as $key => $val) {
            if (!isset($options[$key])) {
                $options[$key] = $val;
            }
        }

        //dump($params, true);

        $endpointsToQuery = [];

        foreach ($options['centers'] as $c) {
            switch ($c) {
                case 'mc':
                    $endpointsToQuery[$c] = 'LstmeetingusageHistory';
                    break;
                case 'tc':
                    $endpointsToQuery[$c] = 'LsttrainingsessionHistory';
                    break;
                case 'sc':
                    $endpointsToQuery[$c] = 'LstsupportsessionHistory';
                    break;
                case 'ec':
                    $endpointsToQuery[$c] = 'LsteventsessionHistory';
                    break;

            }
        }

        $data = [];

        $errorMessages = [];
        foreach ($endpointsToQuery as $c => $e) {
            $orderBy = 'STARTTIME';
            if ($e == 'LstsupportsessionHistory') {
                $orderBy = 'SESSIONSTARTTIME';
            }

            $bodyContent = <<<XML
<listControl>
    <startFrom>0</startFrom>
    <maximumNum>{$options['maximumNum']}</maximumNum>
</listControl>
<order>
    <orderBy>$orderBy</orderBy>
    <orderAD>DESC</orderAD>
</order>
<startTimeScope>
    <sessionStartTimeStart>{$options['createTimeStart']}</sessionStartTimeStart>
    <sessionStartTimeEnd>{$options['createTimeEnd']}</sessionStartTimeEnd>
</startTimeScope>
XML;

            try {
                $result = $this->xmlApiCall('history', $e, $bodyContent);
                $serverResponse = $result['response']['result'];

                if ($serverResponse == 'SUCCESS') {
                    $data[$c] = $result['body'];
                } else {
                    $errorMessages[] = "error retrieving center history for endpoint $e. error: {$result['response']['message']}";
                }
            } catch (WebexApiException $e) {
                // do nothing, it's OK.
            }
        }

        if ($options['collated'] !== true) {
            $sessionsCombined = [];
            foreach ($data as $center => $history) {
                switch ($center) {
                    case 'mc':
                        $centerName = 'meeting';
                        $endpoint = 'meetingUsageHistory';
                        break;
                    case 'tc':
                        $centerName = 'training';
                        $endpoint = 'trainingSessionHistory';
                        break;
                    case 'ec':
                        $centerName = 'event';
                        $endpoint = 'eventSessionHistory';
                        break;
                    case 'sc':
                        $centerName = 'support';
                        $endpoint = 'supportSessionHistory';
                        break;
                    default:
                        $centerName = '';
                        $endpoint = '';
                        break;
                }

                // account for when we just get 1 back, as it is returned not in a parent array
                if ($history['history:matchingRecords']['serv:total'] == 1) {
                    $history['history:' . $endpoint] = [$history['history:' . $endpoint]];
                }

                foreach ($history['history:' . $endpoint] as $session) {
                    // add center name
                    $session['center'] = $centerName;

                    // correct some inconsistencies in API
                    if ($centerName == 'meeting') {
                        $session['history:sessionStartTime'] = $session['history:meetingStartTime'];
                        $session['history:sessionEndTime'] = $session['history:meetingEndTime'];
                    }

                    // add
                    $sessionsCombined[] = $session;
                }
            }
            $data = $sessionsCombined;
        }

        return [
            'response'      => 'SUCCESS',
            'data'          => $data,
            'errorMessages' => $errorMessages
        ];
    }

    /**
     * getSessionsInProgress() - returns a list of sessions currently in progress in WebEx
     *
     * @return array
     */
    public function getSessionsInProgress()
    {
        $bodyContent = <<<XML
        <serviceType>EventCenter</serviceType>
        <serviceType>MeetingCenter</serviceType>
        <serviceType>TrainingCenter</serviceType>
        <serviceType>SupportCenter</serviceType>
XML;

        $result = $this->xmlApiCall('ep', 'LstOpenSession', $bodyContent);

        $sessions = [];

        // parse it out now
        if (!empty($result['body'])) {
            if (isset($result['body']['ep:services']['ep:serviceType'])) {
                // single center
                $toParse[] = $result['body']['ep:services'];
            } else {
                // multiple centers
                $toParse = [];
                foreach ($result['body']['ep:services'] as $service) {
                    $toParse[] = $service;
                }
            }

            foreach ($toParse as $center) {
                if (isset($center['ep:sessions']['ep:sessionKey'])) {
                    // single session
                    $sessions[] = array_merge($center['ep:sessions'], ['center' => $center['ep:serviceType']]);
                } else {
                    // multiple sessions
                    foreach ($center['ep:sessions'] as $session) {
                        $sessions[] = array_merge($session, ['center' => $center['ep:serviceType']]);
                    }
                }
            }
        }

        return [
            'response' => $result['response'],
            'data'     => $sessions
        ];
    }

    /**
     * getPastSession() - gets the details for a past session when given conference ID and center.
     *
     * @param $confID
     * @param $center
     *
     * @return array
     */
    public function getPastSession($confID)
    {
        $centers = [
            'meeting'  => [
                'endpoint'   => 'LstmeetingusageHistory',
                'historyKey' => 'meetingUsageHistory',
            ],
            'training' => [
                'endpoint'   => 'LsttrainingsessionHistory',
                'historyKey' => 'trainingSessionHistory'

            ],
            'event'    => [
                'endpoint'   => 'LsteventsessionHistory',
                'historyKey' => 'eventSessionHistory'
            ],
            'support'  => [
                'endpoint'   => 'LstsupportsessionHistory',
                'historyKey' => 'supportSessionHistory'
            ]
        ];

        $centerFound = false;
        $data = false;

        $exceptionResponses = [];

        foreach ($centers as $name => $centerInfo) {
            $bodyContent = <<<XML
<confID>$confID</confID>
XML;
            try {
                $result = $this->xmlApiCall('history', $centerInfo['endpoint'], $bodyContent);
                $data = $result['body']["history:{$centerInfo['historyKey']}"];
                $centerFound = $name;
                break; // no more search necessary
            } catch (WebexApiException $e) {
                $exceptionResponses[] = $e->getResponse();
            }
        }

        return [
            'center'   => $centerFound,
            'response' => $result['response'] ?? $exceptionResponses,
            'data'     => $data
        ];
    }

    /**
     * getScheduledSession() - gets the details for a scheduled session when given session / meeting Key. NOTE: requires login / acting as user.
     *
     * @param $sessionKey
     * @param $center
     *
     * @return array
     */
    public function getScheduledSession($sessionKey)
    {
        // NOTE: users (including site administrators) cannot do this for meetings that are not their own.
        // shelving this for now.

        $centers = [
            'meeting'  => [
                'endpoint'   => 'GetMeeting',
                'historyKey' => 'meetingUsageHistory',
                'prefix'     => 'meet',
                'key'        => 'meetingKey'
            ],
            'training' => [
                'endpoint'   => 'GetTrainingSession',
                'historyKey' => 'trainingSessionHistory',
                'prefix'     => 'train',
                'key'        => 'sessionKey'

            ],
            'event'    => [
                'endpoint'   => 'GetEvent',
                'historyKey' => 'eventSessionHistory',
                'prefix'     => 'event',
                'key'        => 'sessionKey'

            ]
        ];

        $centerFound = false;
        $data = false;

        foreach ($centers as $name => $centerInfo) {
            $bodyContent = <<<XML
<{$centerInfo['key']}>$sessionKey</{$centerInfo['key']}>
XML;
            // TODO - implement asUser here to make this possible.
            $result = $this->xmlApiCall($name, $centerInfo['endpoint'], $bodyContent);

            if ($result['response']['result'] == 'SUCCESS') {
                $data = $result['body']["{$centerInfo['prefix']}:{$centerInfo['historyKey']}"];
                $centerFound = $name;
                break; // no more search necessary
            }
        }

        return [
            'center'   => $centerFound,
            'response' => $result['response'],
            'data'     => $data
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx Session Action Methods (add / edit / delete)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with modifying session information
    |
    | TODO - add this in
    |
    */

    /*
    |--------------------------------------------------------------------------
    | WebEx Session Attendee Information Methods (get)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with listing / retrieving session attendee information
    |
    |
    */

    public function getSessionAttendeeHistory($confID = false, $center = false)
    {
        $endpoint = false;

        switch ($center) {
            case "mc":
            case "Webex Meetings":
                $endpoint = "LstmeetingattendeeHistory";
                $arrayKey = "history:meetingAttendeeHistory";
                break;
            case "tc":
            case "Webex Training":
                $endpoint = "LsttrainingattendeeHistory";
                $arrayKey = "history:trainingAttendeeHistory";
                break;
            case "ec":
            case "Webex Events":
                $endpoint = "LsteventattendeeHistory";
                $arrayKey = "history:eventAttendeeHistory";
                break;
            case "sc":
            case "Webex Support":
                $endpoint = "LstsupportattendeeHistory";
                $arrayKey = "history:supportAttendeeHistory";
                break;
            default:
                $center = false;
                $arrayKey = false;
                break;
        }

        if ($confID && $center) {
            // initialize stuff for first call
            $currentPosition = 1;
            $fetchNumber = 500; // actual fetch number - set here for initial call. only less than the max if specifically asked for less than the max.

            $stopAt = 0;

            $resultSetSize = 0; // to be set later once we know this value from the first API call.

            //$fetchNumber = 1; // testing

            // init result array
            $returnArray = [];

            // loop it!
            do {
                // set XML
                $bodyContent = <<<XML
            <confID>$confID</confID>
            <listControl>
                <startFrom>$currentPosition</startFrom>
                <maximumNum>$fetchNumber</maximumNum>
                <listMethod>AND</listMethod>
            </listControl>
XML;

                // execute call
                try {
                    $result = $this->xmlApiCall('history', $endpoint, $bodyContent);
                    //                dd($result);
                    $serverResponse = $result['response'];
                    $body = $result['body'];

                    // process
                    // grab the users and append them to the returnArray.
                    $usersReturned = $body[$arrayKey];

                    // if the call retrieves multiple records, use:user will be an array of arrays with user info.
                    // if the call retrieves a single record, use:user will be only the array with user info.
                    // we can get around this by looking at how many records webex tells us it's giving us.

                    if ($body['history:matchingRecords']['serv:returned'] == 1) {
                        // this is a singleton
                        array_push($returnArray, $usersReturned);
                    } else {
                        // grouping
                        $returnArray = array_merge($returnArray, $usersReturned);
                    }

                    if ($resultSetSize == 0) {
                        // if this hasn't been set yet, then set it now.
                        $resultSetSize = $body['history:matchingRecords']['serv:total'];
                        if ($stopAt == 0) {
                            // set this also if it wasn't set before. this'll stop at the size of the resultset.
                            $stopAt = $resultSetSize;
                        }
                    }

                    $currentPosition += $fetchNumber; // in case pagination is needed. if it's not needed this won't matter anyway.

                    $arrayCount = count($returnArray);
                    if ($stopAt - $arrayCount < $fetchNumber) {
                        $fetchNumber = $stopAt - $arrayCount;
                    }
                } catch (WebexApiException $e) {
                    $stopAt = $currentPosition;
                }
            } while ($currentPosition < $stopAt);


            // append count of data to server response info
            $serverResponse['recordsFetched'] = count($returnArray);

//            $result = $this->xmlApiCall("history", $endpoint, $bodyContent);

//            $serverResponse = $result["response"];
//            $data = $result["body"]["serv:message"]["serv:body"]["serv:bodyContent"];

            return [
                "response" => $serverResponse,
                "data"     => $returnArray
            ];
        }

        return [
            "response" => "error: no meeting key provided, or invalid center value provided.",
            "data"     => null
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | WebEx Session Attendee Action Methods (add / edit / delete)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with modifying session attendee information
    |
    | TODO - add this in
    |
    */

    /*
    |--------------------------------------------------------------------------
    | WebEx Tracking Code Information Methods (get)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with listing / retrieving tracking code information
    |
    | TODO - add this in
    |
    */

    /*
    |--------------------------------------------------------------------------
    | WebEx Tracking Code Action Methods (add / edit / delete)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with modifying tracking code information
    |
    | TODO - add this in
    |
    */

    /*
    |--------------------------------------------------------------------------
    | WebEx Telephony Information Methods (get)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with listing / retrieving telephony information
    |
    | TODO - add this in
    |
    */

    /*
    |--------------------------------------------------------------------------
    | WebEx Telephony Action Methods (add / edit / delete)
    |--------------------------------------------------------------------------
    |
    | Operations that deal with modifying telephony information
    |
    | TODO - add this in
    |
    */
}
