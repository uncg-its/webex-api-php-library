<?php

namespace Uncgits\WebexApi\Exceptions;

class WebexApiException extends \Exception
{
    protected $response;

    public function __construct($message = null, $code = 0, $previous = null, $response = [])
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
        $this->message = 'Webex API error (' . $this->response['response']['exceptionId'] . '): ' . $this->response['response']['message'];
        if ($message !== null) {
            $this->message .= '; Additional information: ' . $message;
        }
    }

    public function getResponse()
    {
        return $this->response;
    }
}
