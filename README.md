# Webex API PHP Library

Contact: matt.libera@uncg.edu

# Introduction

This package is a PHP library used to interact with the Webex XML and NBR APIs.

> This is a **work in progress**. Not recommend for production apps just yet.

## Scope

This package is built not (yet) as a comprehensive interface with the Webex API - it is not written to perform every conceivable API call to Webex. Specifically, this package was built to suit the needs of UNC Greensboro's Webex environment, and performs only the functions necessary for day-to-day operations of Webex at UNCG.

Please fork and add methods to this as you see fit / as your needs dictate.

# Installation

1. `composer require 'uncgits/webex-api-php-library'`
2. Use one of the API calls built into the `Uncgits\WebexApi\WebexApi` class, or extend it and add your own. The XML API call is built in there. You'll have to find a way to call the setter methods to set the variables required to make the call - specific to your environment (e.g. urls, credentials, etc.)

# Version History

### 0.6.2

- change to PSR-4 declaration since we were already following it

### 0.6.1

- Fix for when we get a single session back from a center in `getPastSessions()` - avoids an "Illegal string offset 'center'" error

### 0.6.0

- API now throws `WebexApiException` when a FAILURE result is received from the endpoint
- Handling of exception-throwing in some paginated calls (`getPastSessions`, and `getSessionAttendeeHistory`)

### 0.5.1

- sets proxy explicitly to empty string in `$requestOptions` array when not being used, so that we don't fall back on the environment default.

### 0.5

- adds `addUser()` and `deleteUser()` methods
- standardizing how response is generated in `xmlApiCall()` method to reduce over-complicated and unnecessary extra parsing in child methods
- `editUser()` method changed to apply both `$attributes` and `$customXml` if provided
- starting general code cleanup (PSR2) and better docblocks

### 0.4

- adds working method `getScheduledSessions()` and half-working method `getScheduledSession()`

### 0.3

- `WebexApi` object's properties set to protected instead of private

### 0.2

- changing history months to history days.

### 0.1.1

- Adds 'collated' option to session info / history listing - now returns all sessions in one parseable array unless told to collate.

### 0.1

First real "release" (still beta). Available functionality:

- XML API call abstraction
- Site info (site.GetSite)
- API Version (ep.GetAPIVersion)
- Users list (user.LstsummaryUser) - all / active-only
- User info (user.GetUser) - account info / active status
- Edit user (user.SetUser) - name, email, WebExId, password only for now
- Session info (history.Lst*sessionHistory)
